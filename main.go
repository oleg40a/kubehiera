package main

import (
	goflags "github.com/jessevdk/go-flags"
	"os"
)

type Options struct {
	Verbose   []bool `short:"v" long:"verbose" description:"Show verbose debug information"`
	HieraPath string `short:"p" long:"path" default:"data/hiera.yml" description:"path to the hiera.yml file"`
}

var options Options
var parser = goflags.NewParser(&options, goflags.Default)

func main() {

	if _, err := parser.Parse(); err != nil {
		if flagsErr, ok := err.(*goflags.Error); ok && flagsErr.Type == goflags.ErrHelp {
			os.Exit(0)
		} else {
			os.Exit(1)
		}
	}
}
