package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/base64"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"log"
	"math/big"
	"os"
	"path"
	"time"

	"github.com/msvechla/pkcs7"
)

type CryptoBackend interface {
	encryptValue(val string) string
	decryptValue(val string) string
	setup()
	init()
}

type EnvVarSecret struct {
}

func (e EnvVarSecret) setup() {
}

func (e EnvVarSecret) init() {
}

func (e EnvVarSecret) encryptValue(val string) string {
	return ""
}

func (e EnvVarSecret) decryptValue(val string) string {
	return fmt.Sprintf("'%s'", os.Getenv(val))
}

// PKCS7Encryption implements cryptography for Kubehiera
type PKCS7Encryption struct {
	Config      map[string]string
	OutputDir   string
	PrivateKey  interface{}
	Certificate *x509.Certificate
}

func (c *PKCS7Encryption) init(basepath string) {

	if pubKeyPath, exists := c.Config["public_key"]; exists {
		pubKeyFile, err := ioutil.ReadFile(fmt.Sprintf("%s/%s", basepath, pubKeyPath))
		if err != nil {
			log.Fatalf("Unable to open public_key: %s\n", err)
		}
		block, _ := pem.Decode([]byte(pubKeyFile))
		if block == nil {
			log.Fatalln("failed to decode certificate PEM")
		}

		c.Certificate, err = x509.ParseCertificate(block.Bytes)
		if err != nil {
			log.Fatalf("failed to parse certificate: %s", err.Error())
		}
	}

	if privKeyPath, exists := c.Config["private_key"]; exists {
		privKeyFile, err := ioutil.ReadFile(fmt.Sprintf("%s/%s", basepath, privKeyPath))
		if err != nil {
			log.Fatalf("Unable to open privatec_key: %s\n", err)
		}
		privBlock, _ := pem.Decode([]byte(privKeyFile))
		if privBlock == nil {
			log.Fatalln("failed to decode certificate PEM")
		}

		privKey, err := x509.ParsePKCS1PrivateKey(privBlock.Bytes)
		if err != nil {
			log.Fatalf("failed to parse private key: %s", err.Error())
		}

		c.PrivateKey = privKey
	}
}

func (c *PKCS7Encryption) setup() {

	log.Print("generating certificate and private key..")

	RSAPrivateKey, err := rsa.GenerateKey(rand.Reader, 4096)

	if err != nil {
		log.Printf("Error generating RSA Key: %s", err)
	}

	notBefore := time.Now()
	notAfter := notBefore.Add(100 * 365 * 24 * time.Hour)

	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		log.Fatalf("failed to generate serial number: %s", err)
	}

	template := x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			Organization: []string{"Acme Co"},
		},
		IsCA:                  true,
		NotBefore:             notBefore,
		NotAfter:              notAfter,
		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}

	certBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, &RSAPrivateKey.PublicKey, RSAPrivateKey)
	if err != nil {
		log.Fatalf("Failed to create certificate: %s", err)
	}

	certOutDir := fmt.Sprintf("%s/cert.pem", path.Dir(c.OutputDir))
	certOut, err := os.Create(certOutDir)
	if err != nil {
		log.Fatalf("failed to open %s for writing: %s", certOutDir, err)
	}
	pem.Encode(certOut, &pem.Block{Type: "CERTIFICATE", Bytes: certBytes})
	certOut.Close()
	log.Printf("written %s\n", certOutDir)

	keyOutDir := fmt.Sprintf("%s/key.pem", path.Dir(c.OutputDir))
	keyOut, err := os.OpenFile(keyOutDir, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("failed to open %s for writing: %s", keyOutDir, err)
		return
	}
	pem.Encode(keyOut, &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(RSAPrivateKey)})
	keyOut.Close()
	log.Printf("written %s\n", keyOutDir)

}

func (c *PKCS7Encryption) decryptValue(ciphertext string) string {

	data, err := base64.StdEncoding.DecodeString(ciphertext)
	if err != nil {
		log.Fatalf("Error decoding base64: %s", err)
	}

	pkcs7Crypto, err := pkcs7.Parse(data)
	if err != nil {
		log.Fatalf("error parsing cipher: %s", err)
	}

	plaintext, err := pkcs7Crypto.Decrypt(c.Certificate, c.PrivateKey)
	if err != nil {
		log.Fatalf("error decrypting cipher: %s", err)
	}
	return fmt.Sprintf("'%s'", string(plaintext))
}

func (c *PKCS7Encryption) encryptValue(plaintext string) string {
	pkcs7.ContentEncryptionAlgorithm = pkcs7.EncryptionAlgorithmAES256CBC
	ciphertext, _ := pkcs7.Encrypt([]byte(plaintext), []*x509.Certificate{c.Certificate})
	return fmt.Sprintf("ENC[PKCS7,%s]", base64.StdEncoding.EncodeToString(ciphertext))
}
