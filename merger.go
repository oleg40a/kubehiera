package main

import (
	"bytes"
	"fmt"
	yaml "github.com/Navops/yaml"
	"github.com/imdario/mergo"
	"io/ioutil"
	"log"
	"os"
	"path"
	"strings"
	"text/template"
	//yaml "gopkg.in/yaml.v2" // waiting for FIXME: https://github.com/go-yaml/yaml/issues/139
)

const ENC_PREFIX = "ENC["

type HieraFile struct {
	Hierarchy      []string                     `yaml:",flow"`
	Cryptobackends map[string]map[string]string `yaml:",flow"`
}

type Merger struct {
	HieraPath      string
	IdentityData   []byte
	MergerIdentity interface{}
	MergedHiera    map[string]interface{}
	HieraData      HieraFile
	CryptoPKCS7    PKCS7Encryption
	Output         string
	Input          string
}

func (m *Merger) init() {
	m.readHiera()
	m.setupCryptoBackends()
}

func (m *Merger) run() {
	m.mergeHiera()
	m.decryptHiera()
	m.renderHiera()
}

func (m *Merger) readHiera() {
	// trim whitespace
	if m.MergerIdentity != nil {
		m.MergerIdentity = trimWhitespace(m.MergerIdentity)
	}
	if len(options.Verbose) > 0 {
		log.Printf("EnvironmentOptions: %s\n", m.MergerIdentity)
	}

	// parse hiera.yaml as template
	t, err := template.ParseFiles(m.HieraPath)
	if err != nil {
		log.Println("executing template:", err)
	}

	var hieraBuffer bytes.Buffer
	error := t.Execute(&hieraBuffer, m.MergerIdentity)
	if error != nil {
		log.Println("Error rendering MergerIdentity:", error)
	}

	error2 := yaml.Unmarshal([]byte(hieraBuffer.Bytes()), &m.HieraData)
	if error2 != nil {
		log.Println("Error parsing hiera yaml:", error2)
	}

	rangeList := m.HieraData.Hierarchy
	var tempList []string

	// cleanup items from hierarchy that are unset
	for _, item := range rangeList {

		fullpath := fmt.Sprintf("%s/%s", path.Dir(m.HieraPath), item)

		if _, err := os.Stat(fullpath); !os.IsNotExist(err) {
			tempList = append(tempList, fullpath)
		}
	}

	// overwrite the parsed yaml data with the resulting cleaned-up list
	m.HieraData.Hierarchy = tempList
}

func (m *Merger) setupCryptoBackends() {
	for backend, options := range m.HieraData.Cryptobackends {
		switch backend {
		case "pkcs7":
			m.CryptoPKCS7 = PKCS7Encryption{Config: options}
			m.CryptoPKCS7.init(path.Dir(m.HieraPath))
		default:
			fmt.Printf("Unrecognized Cryptobackend: %s", backend)
		}

	}
}

func (m *Merger) mergeHiera() {

	dstFile, _ := ioutil.ReadFile(m.HieraData.Hierarchy[len(m.HieraData.Hierarchy)-1])
	var dstMap, srcMap map[string]interface{}
	err := yaml.Unmarshal(dstFile, &dstMap)
	if err != nil {
		log.Println("Error parsing yaml:", err)
	}
	//log.Println(m.HieraData.Hierarchy)
	//log.Println(dstMap)

	for i := len(m.HieraData.Hierarchy) - 2; i >= 0; i-- {
		//log.Println(i)
		srcFile, _ := ioutil.ReadFile(m.HieraData.Hierarchy[i])
		err := yaml.Unmarshal(srcFile, &srcMap)
		if err != nil {
			log.Println("Error parsing yaml:", err)
		}

		mergo.Merge(&dstMap, srcMap)
	}

	m.MergedHiera = dstMap
}

func (m *Merger) decryptHiera() {
	encHiera, _ := yaml.Marshal(m.MergedHiera)
	decHiera := m.recursiveDecrypt(string(encHiera))
	yaml.Unmarshal([]byte(decHiera), &m.MergedHiera)
}

func (m *Merger) recursiveDecrypt(s string) string {
	if strings.Contains(s, ENC_PREFIX) {
		// get the first ENC[CRYPTOBACKEND,VAL] pair
		nextEncString := s[strings.Index(s, ENC_PREFIX)+len(ENC_PREFIX) : strings.Index(s[strings.Index(s, ENC_PREFIX):], "]")+strings.Index(s, ENC_PREFIX)]
		crypt := strings.Split(nextEncString, ",")
		switch crypt[0] {
		case "ENV":
			return m.recursiveDecrypt(strings.Replace(s, fmt.Sprintf("%s%s]", ENC_PREFIX, nextEncString), EnvVarSecret{}.decryptValue(crypt[1]), -1))
		case "PKCS7":
			return m.recursiveDecrypt(strings.Replace(s, fmt.Sprintf("%s%s]", ENC_PREFIX, nextEncString), m.CryptoPKCS7.decryptValue(crypt[1]), -1))
		default:
			log.Fatalf("Cryptobackend: %s not found!", crypt[0])
		}
	}
	return s
}

func (m *Merger) renderHiera() {
	t, err := template.ParseFiles(m.Input)
	if err != nil {
		log.Println("executing template:", err)
	}

	if len(options.Verbose) > 0 {
		log.Printf("MergedHiera: %s\n", m.MergedHiera)
	}

	if m.Output != "" {
		outfile, err := os.Create(m.Output)
		if err != nil {
			log.Fatalf("failed to open %s for writing: %s", m.Output, err)
		}

		error := t.Execute(outfile, m.MergedHiera)
		if error != nil {
			log.Println("Error rendering MergerIdentity:", error)
		}
	} else {
		error := t.Execute(os.Stdout, m.MergedHiera)
		if error != nil {
			log.Println("Error rendering MergerIdentity:", error)
		}
	}

}

func trimWhitespace(dat interface{}) interface{} {
	untrimmedDat := dat.(map[string]string)
	var trimmedDat = make(map[string]string)
	for k, v := range untrimmedDat {
		trimmedDat[strings.TrimSpace(k)] = strings.TrimSpace(v)
	}
	return trimmedDat
}
