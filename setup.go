package main

type Setup struct {
	Output        string `short:"o" long:"output" description:"Output destination folder" defaul:"."`
	CryptoBackend string `short:"c" long:"cryptobackend" description:"CryptoBackend to setup" choice:"PKCS7" default:"PKCS7"`
}

var setup Setup

func (s *Setup) Execute(args []string) error {
	switch s.CryptoBackend {
	case "PKCS7":
		c := PKCS7Encryption{OutputDir: s.Output}
		c.setup()
	}
	return nil
}

func init() {
	parser.AddCommand("setup", "setup a CryptoBackend", "generates all necessary files for a CryptoBackend", &setup)
}
