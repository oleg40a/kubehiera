package main

import (
	"crypto/md5"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"testing"
)

func TestRender(t *testing.T) {

	tables := []struct {
		TestFile         string
		VerificationFile string
		Environment      map[string]string
	}{
		{"example/secret.yml", "example/output/secret_test.yml", map[string]string{"environment": "test"}},
		{"example/secret.yml", "example/output/secret_fra02_prod.yml", map[string]string{"environment": "prod", "datacenter": "fra02"}},
		{"example/secret.yml", "example/output/secret_ams01_prod.yml", map[string]string{"environment": "prod", "datacenter": "ams01"}},
		{"example/configmap.yml", "example/output/configmap_test.yml", map[string]string{"environment": "test"}},
		{"example/configmap.yml", "example/output/configmap_fra02_prod.yml", map[string]string{"environment": "prod", "datacenter": "fra02"}},
		{"example/configmap.yml", "example/output/configmap_ams01_prod.yml", map[string]string{"environment": "prod", "datacenter": "ams01"}},
	}

	dir, err := ioutil.TempDir("", "rendertest")
	if err != nil {
		log.Fatal(err)
	}

	defer os.RemoveAll(dir)

	// set env passwords
	os.Setenv("PASS1", "mySecretPass1")
	os.Setenv("PASS2", "mySecretPass2")

	for _, table := range tables {

		r := &Render{
			Environment: table.Environment,
			Output:      fmt.Sprintf("%s/%s", dir, filepath.Base(table.TestFile)),
			RenderOpts: struct {
				TemplateFile string
			}{
				TemplateFile: table.TestFile,
			},
		}

		options.HieraPath = "example/data/hiera.yml"
		r.Execute([]string{})

		// hash of newly generated file
		fTest, err := os.Open(fmt.Sprintf("%s/%s", dir, filepath.Base(table.TestFile)))
		if err != nil {
			log.Fatalf("Error opening file: %s", err)
		}
		defer fTest.Close()

		hTest := md5.New()
		if _, err := io.Copy(hTest, fTest); err != nil {
			log.Fatalf("Error generating hash: %s", err)
		}

		// hash of previously generated correct file
		fReal, err := os.Open(table.VerificationFile)
		if err != nil {
			log.Fatalf("Error opening file: %s", err)
		}
		defer fReal.Close()

		hReal := md5.New()
		if _, err := io.Copy(hReal, fReal); err != nil {
			log.Fatalf("Error generating hash: %s", err)
		}

		hashTest := string(hTest.Sum(nil))
		hashReal := string(hReal.Sum(nil))

		if hashTest != hashReal {
			t.Fatalf("hash of test file: %x and previously generated correct file: %x do not match!", hashTest, hashReal)
		}
	}

}
