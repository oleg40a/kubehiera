# Kubehiera

Lightweight tool to render go template files based on hierarchical data. Support for multiple encryption backends and fully compatible with hiera-eyaml. Designed with rendering Kubernetes deployments in mind and inspired by the well-known hiera.

## Getting Started

A good way of getting started is by looking at one of the examples provided in the [example folder](./example/). As a bare-minium you will need a hiera.yml file. You can also see usage information by running `./kubehiera --help`

### Download the latest release

We provide automated builds for all major platforms. You can find all releases in our [tags](https://gitlab.com/msvechla/kubehiera/tags) section. Simply click on downloads and select "artifacts" to get the desired version. Alternatively feel free to build the latest release from master yourself.

### Setting up your hierarchy

Kubehiera requires one single configuration file, which defines the hierarchy used when rendering your templates and configuration for crypt backends.

The hiera.yml file contains the following configuration:

| Config Key       | Type          | Description                |
| ---------------- |:-------------:| -------------------------- |
| `hierarchy`      | array         | Dynamic list of data files |
| `cryptobackends` | map           | Currently supports `pkcs7` |

```yaml
---
hierarchy:
  - datacenter/{{ .datacenter }}.yml
  - environment/{{ .environment }}.yml
  - common.yml

cryptobackends:
  pkcs7:
    private_key: "key.pem"
    public_key: "cert.pem"
```
*Examplary hiera.yml file from [example/data/hiera.yml](./example/data/hiera.yml)*

`hierarchy` is read from bottom to top. Configuration at a higher level overrides configuration at a lower hierarchical level. You can use go-template syntax to define input data files based on variables.

`cryptobackends` currently only supplies coniguration for the pkcs7 cryotobackend. `private_key`and `public_key` contain the relative paths to the correspondent files starting from the location of the hiera.yml
### Rendering Templates

The main focus of Kubehiera is to render template files based on hierarchical data. This is done by specifying the path to your hiera.yml, the template file you want to render and some environment configuration:

```
./kubehiera render -p example/data/hiera.yml example/configmap.yml -e "environment:prod" -e "datacenter:ams01"
```
This will render the template file `example/configmap.yml` by using values defined in the data files, which are referenced in the `example/data/hiera.yml`. Additionally we supply environment configuration with the `-e <key:value>` option. This will be used when walking the hiera.yml to select the desired data.

## Encrypting sensitive data using CryptoBackends

Currently Kubehiera support two different CryptoBackends. **ENV** and **PKCS7**. CryptoBackends can be used in any of your data yaml files by stating ```ENC[BACKEND,VALUE]```. For more details on the specific backends, see below.

### ENV CryptoBackend
The ENV CryptoBackend allows you to retrieve sensitive data from environment variables. This has been established as best practice for example by the [twelve-factor app](https://12factor.net/).

To use the data from the "password" environment variable inside any of your data yaml files, you can simply use: ```ENC[ENV,password]```. During the rendering process, this will then be replaced with the actual value.

### PKCS7 CryptoBackend
The PKCS7 backend can be used to encrypt sensitive data, which then can safely be stored inside your git repository. The CryptoBackend is fully compatible with [hiera-eyaml](https://github.com/voxpupuli/hiera-eyaml), so you can use your existing encrypted hiera data files with Kubehiera  by specifying your keys inside the ```hiera.yml```.

If you do not have any existing keypairs, you can generate them by specifying ```./kubehiera setup -c pkcs7```. After configuring them inside your ```hiera.yml``` you are ready to use encrypted values inside your data yaml files.

To encrypt data you can use the integrated encryption tool:
```
./kubehiera encrypt -s "HelloThisIsASecret" -p example/data/hiera.yml
```
Afterwards you can use the encrypted value in any of your hiera data files, e.g.:
```yaml
---
mypass: ENC[PKCS7,MIICqgYJKoZ...]
```

## Contributing

Please read [CONTRIBUTING.md]() for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/msvechla/kubehiera/tags).

## Authors

* **Marius Svechla** - *Initial work*

See also the list of [contributors](https://gitlab.com/msvechla/kubehiera/graphs/master) who participated in this project.

## License


## Acknowledgments

* [Hiera](https://github.com/puppetlabs/hiera) - for being the inspiration of this tool
