package main

import "fmt"

type Encrypt struct {
	CryptoBackend string `short:"c" long:"cryptobackend" description:"CryptoBackend to setup" choice:"PKCS7" default:"PKCS7"`
	String        string `short:"s" long:"string" description:"String to encrypt" required:"true"`
}

var encrypt Encrypt

func (e *Encrypt) Execute(args []string) error {
	m := Merger{HieraPath: options.HieraPath}
	m.init()
	var ciphertext string
	switch e.CryptoBackend {
	case "PKCS7":
		ciphertext = m.CryptoPKCS7.encryptValue(e.String)
	}

	fmt.Println(ciphertext)
	return nil
}

func init() {
	parser.AddCommand("encrypt", "encrypts a value", "encrypts a value with the selected CryptoBackend", &encrypt)
}
